import React from 'react';
import avion from './avion_tourne.png';
import './App.css';
import App1 from './app-fictive-1/app1';
import App2 from './app-fictive-2/app2';
import App3 from './app-fictive-3/app3';
import App4 from './app-fictive-4/app4';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={avion} className="App-logo" alt="logo" />
        <p>
          Bienvenue sur les application fictive du projet ATM
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <App1 />
      <App2 appconf="Ceci est un texte"/>
      <App3 />
      <App4 appconf="19h30"/>


    </div>
  );
}

export default App;
